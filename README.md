Dr Baldeep Farmah is GMC registered and Harley Street trained to the highest level in cosmetic procedures. Our Doctor-led services use the highest quality products and industry leading treatments. At Dr Aesthetica, client safety and excellence in all cosmetic treatments is our number 1 priority.

Address: Unit 1, 1431-1433 Bristol Road South, Northfield, Birmingham, West Midlands B31 2SU, UK

Phone: +44 121 769 0242

